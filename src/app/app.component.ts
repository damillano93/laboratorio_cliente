import { Component, OnInit } from '@angular/core';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';
import { SidebarjsService, SidebarConfig } from 'ng-sidebarjs';
import { TipoService } from './services/tipo.service';
import { UbicacionService } from './services/ubicacion.service';
import { SedeService } from './services/sede.service';
import { DisponibilidadService } from './services/disponibilidad.service';
import { LaboratorioService } from './services/laboratorio.service';
import { PersonaService } from './services/persona.service';

import { NavigationCancel,
        Event,
        NavigationEnd,
        NavigationError,
        NavigationStart,
        Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'laboratorio';
  version;
  constructor(
    public readonly sidebarjsService: SidebarjsService,
    private _loadingBar: SlimLoadingBarService,
    private _router: Router,
    private TipoSer: TipoService,
    private UbicacionSer: UbicacionService,
    private SedeSer: SedeService,
    private DisponibilidadSer: DisponibilidadService,
    private LaboratorioSer: LaboratorioService,
    private PersonaSer: PersonaService, ) {
    this._router.events.subscribe((event: Event) => {
      this.navigationInterceptor(event);
    });
  }

  ngOnInit() {  }
  private navigationInterceptor(event: Event): void {
    if (event instanceof NavigationStart) {
      this._loadingBar.start();
    }
    if (event instanceof NavigationEnd) {
      this._loadingBar.complete();
    }
    if (event instanceof NavigationCancel) {
      this._loadingBar.stop();
    }
    if (event instanceof NavigationError) {
      this._loadingBar.stop();
    }
  }
}
