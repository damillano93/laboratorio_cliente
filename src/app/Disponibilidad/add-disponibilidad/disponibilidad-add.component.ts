import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Nombre_dia from '../../models/Nombre_dia'; import Laboratorio from '../../models/Laboratorio';
import { DisponibilidadService } from '../../services/disponibilidad.service';
import { NombrediaService } from '../../services/nombre_dia.service';
import { LaboratorioService } from '../../services/laboratorio.service';
import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-disponibilidad-add',
  templateUrl: './disponibilidad-add.component.html',
  styleUrls: ['./disponibilidad-add.component.css']
})
export class DisponibilidadAddComponent implements OnInit {
   Nombre_dia: Nombre_dia[]; Laboratorio: Laboratorio[];   angForm: FormGroup;
  constructor(
private Nombre_diaSer: NombrediaService, private LaboratorioSer: LaboratorioService,
   private fb: FormBuilder,
   private Disponibilidad_ser: DisponibilidadService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       fecha: ['', Validators.required ],
dia: ['', Validators.required ],
nombre_dia: ['', Validators.required ],
laboratorio: ['', Validators.required ],
descripcion: ['', Validators.required ],
responsable: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addDisponibilidad(fecha, dia, nombre_dia, laboratorio, descripcion, responsable ) {
    const now = new Date();

    const otro = new Date(fecha);

    const name_day = otro.getDay();
    console.log(name_day);
const start = new Date(now.getFullYear(), 0, 0);
const  diff = Math.abs(otro.getTime() - start.getTime());
const oneDay = 1000 * 60 * 60 * 24;
const day = Math.floor(diff / oneDay);

    dia = day;
    nombre_dia = this.Nombre_dia[name_day - 1];
    console.log(nombre_dia - 1);
    this.Disponibilidad_ser.addDisponibilidad(fecha, dia, nombre_dia, laboratorio, descripcion, responsable );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
    this.Nombre_diaSer
.getNombre_dia()
.subscribe((data: Nombre_dia[]) => {
this.Nombre_dia = data;
});
this.LaboratorioSer
.getLaboratorio()
.subscribe((data: Laboratorio[]) => {
this.Laboratorio = data;
});





  }

}
