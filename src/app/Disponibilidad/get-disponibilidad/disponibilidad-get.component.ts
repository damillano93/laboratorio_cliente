import { Component, OnInit } from '@angular/core';
import Disponibilidad from '../../models/Disponibilidad';
import { DisponibilidadService } from '../../services/disponibilidad.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-disponibilidad-get',
  templateUrl: './disponibilidad-get.component.html',
  styleUrls: ['./disponibilidad-get.component.css']
})

export class DisponibilidadGetComponent implements OnInit {

  disponibilidad: Disponibilidad[];

  constructor( private bs: DisponibilidadService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getDisponibilidad()
      .subscribe((data: Disponibilidad[]) => {
        this.disponibilidad = data;
    });
  }

  deleteDisponibilidad(id) {
    this.bs.deleteDisponibilidad(id).subscribe(res => {
      console.log('Deleted');
      this.bs
      .getDisponibilidad()
      .subscribe((data: Disponibilidad[]) => {
        this.disponibilidad = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

