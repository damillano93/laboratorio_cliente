import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Nombre_dia from '../../models/Nombre_dia'; import Laboratorio from '../../models/Laboratorio';
import { DisponibilidadService } from '../../services/disponibilidad.service';
import { NombrediaService } from '../../services/nombre_dia.service';
import { LaboratorioService } from '../../services/laboratorio.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-disponibilidad-edit',
  templateUrl: './disponibilidad-edit.component.html',
  styleUrls: ['./disponibilidad-edit.component.css']
})
export class DisponibilidadEditComponent implements OnInit {
  Nombre_dia: Nombre_dia[]; Laboratorio: Laboratorio[];   angForm: FormGroup;
  disponibilidad: any = {};

  constructor(private route: ActivatedRoute, private Nombre_diaSer: NombrediaService, private LaboratorioSer: LaboratorioService,
    private router: Router,
    public toastr: ToastrManager,
    private bs: DisponibilidadService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        fecha: ['', Validators.required ],
dia: ['', Validators.required ],
nombre_dia: ['', Validators.required ],
laboratorio: ['', Validators.required ],
descripcion: ['', Validators.required ],
responsable: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editDisponibilidad(params['id']).subscribe(res => {
        this.disponibilidad = res;
      });
    });
     this.Nombre_diaSer
.getNombre_dia()
.subscribe((data: Nombre_dia[]) => {
this.Nombre_dia = data;
});
this.LaboratorioSer
.getLaboratorio()
.subscribe((data: Laboratorio[]) => {
this.Laboratorio = data;
});
   }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateDisponibilidad(fecha, dia, nombre_dia, laboratorio, descripcion, responsable ) {
   this.route.params.subscribe(params => {
      this.bs.updateDisponibilidad(fecha, dia, nombre_dia, laboratorio, descripcion, responsable  , params['id']);
      this.showInfo();
      this.router.navigate(['disponibilidad']);
   });
}
}
