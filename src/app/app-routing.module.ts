import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CalendarioComponent } from './Calendario/calendario.component';
import { TipoAddComponent } from './Tipo/add-tipo/tipo-add.component';
import { TipoEditComponent } from './Tipo/edit-tipo/tipo-edit.component';
import { TipoGetComponent } from './Tipo/get-tipo/tipo-get.component';
import { UbicacionAddComponent } from './Ubicacion/add-ubicacion/ubicacion-add.component';
import { UbicacionEditComponent } from './Ubicacion/edit-ubicacion/ubicacion-edit.component';
import { UbicacionGetComponent } from './Ubicacion/get-ubicacion/ubicacion-get.component';
import { SedeAddComponent } from './Sede/add-sede/sede-add.component';
import { SedeEditComponent } from './Sede/edit-sede/sede-edit.component';
import { SedeGetComponent } from './Sede/get-sede/sede-get.component';
import { DisponibilidadAddComponent } from './Disponibilidad/add-disponibilidad/disponibilidad-add.component';
import { DisponibilidadEditComponent } from './Disponibilidad/edit-disponibilidad/disponibilidad-edit.component';
import { DisponibilidadGetComponent } from './Disponibilidad/get-disponibilidad/disponibilidad-get.component';
import { LaboratorioAddComponent } from './Laboratorio/add-laboratorio/laboratorio-add.component';
import { LaboratorioEditComponent } from './Laboratorio/edit-laboratorio/laboratorio-edit.component';
import { LaboratorioGetComponent } from './Laboratorio/get-laboratorio/laboratorio-get.component';
import { CalendarioEditComponent } from './Calendario/edit-calendario/calendario-edit.component';
const routes: Routes = [
  {
  path: '',
  component: HomeComponent
},
{
  path: 'calendario',
  component: CalendarioComponent
},
{
  path: 'calendario/edit/:id',
  component: CalendarioEditComponent
},
  {
  path: 'tipo/create',
  component: TipoAddComponent
},
{
  path: 'tipo/edit/:id',
  component: TipoEditComponent
},
{
  path: 'tipo',
  component: TipoGetComponent
},
{
  path: 'ubicacion/create',
  component: UbicacionAddComponent
},
{
  path: 'ubicacion/edit/:id',
  component: UbicacionEditComponent
},
{
  path: 'ubicacion',
  component: UbicacionGetComponent
},
{
  path: 'sede/create',
  component: SedeAddComponent
},
{
  path: 'sede/edit/:id',
  component: SedeEditComponent
},
{
  path: 'sede',
  component: SedeGetComponent
},
{
  path: 'disponibilidad/create',
  component: DisponibilidadAddComponent
},
{
  path: 'disponibilidad/edit/:id',
  component: DisponibilidadEditComponent
},
{
  path: 'disponibilidad',
  component: DisponibilidadGetComponent
},
{
  path: 'laboratorio/create',
  component: LaboratorioAddComponent
},
{
  path: 'laboratorio/edit/:id',
  component: LaboratorioEditComponent
},
{
  path: 'laboratorio',
  component: LaboratorioGetComponent
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
