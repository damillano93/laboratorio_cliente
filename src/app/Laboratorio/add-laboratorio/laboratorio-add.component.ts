import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Tipo from '../../models/Tipo';
import Ubicacion from '../../models/Ubicacion';
import Sede from '../../models/Sede';
import { LaboratorioService } from '../../services/laboratorio.service';
import { TipoService } from '../../services/tipo.service';
import { UbicacionService } from '../../services/ubicacion.service';
import { SedeService } from '../../services/sede.service';
import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-laboratorio-add',
  templateUrl: './laboratorio-add.component.html',
  styleUrls: ['./laboratorio-add.component.css']
})
export class LaboratorioAddComponent implements OnInit {
   Tipo: Tipo[]; Ubicacion: Ubicacion[]; Sede: Sede[];   angForm: FormGroup;
  constructor(
private TipoSer: TipoService, private UbicacionSer: UbicacionService, private SedeSer: SedeService,
   private fb: FormBuilder,
   private Laboratorio_ser: LaboratorioService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombre: ['', Validators.required ],
estado: ['', Validators.required ],
tipo: ['', Validators.required ],
ubicacion: ['', Validators.required ],
sede: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addLaboratorio(nombre, estado, tipo, ubicacion, sede ) {
    this.Laboratorio_ser.addLaboratorio(nombre, estado, tipo, ubicacion, sede );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
    this.TipoSer
.getTipo()
.subscribe((data: Tipo[]) => {
this.Tipo = data;
});
this.UbicacionSer
.getUbicacion()
.subscribe((data: Ubicacion[]) => {
this.Ubicacion = data;
});
this.SedeSer
.getSede()
.subscribe((data: Sede[]) => {
this.Sede = data;
});
  }

}
