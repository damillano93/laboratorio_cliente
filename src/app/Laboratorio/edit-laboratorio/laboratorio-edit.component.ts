import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Tipo from '../../models/Tipo';
import Ubicacion from '../../models/Ubicacion';
import Sede from '../../models/Sede';
import { LaboratorioService } from '../../services/laboratorio.service';
import { TipoService } from '../../services/tipo.service';
import { UbicacionService } from '../../services/ubicacion.service';
import { SedeService } from '../../services/sede.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-laboratorio-edit',
  templateUrl: './laboratorio-edit.component.html',
  styleUrls: ['./laboratorio-edit.component.css']
})
export class LaboratorioEditComponent implements OnInit {
  Tipo: Tipo[]; Ubicacion: Ubicacion[]; Sede: Sede[];   angForm: FormGroup;
  laboratorio: any = {};

  constructor(private route: ActivatedRoute,
    private TipoSer: TipoService,
    private UbicacionSer: UbicacionService,
    private SedeSer: SedeService,
    private router: Router,
    public toastr: ToastrManager,
    private bs: LaboratorioService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombre: ['', Validators.required ],
estado: ['', Validators.required ],
tipo: ['', Validators.required ],
ubicacion: ['', Validators.required ],
sede: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editLaboratorio(params['id']).subscribe(res => {
        this.laboratorio = res;
      });
    });
     this.TipoSer
.getTipo()
.subscribe((data: Tipo[]) => {
this.Tipo = data;
});
this.UbicacionSer
.getUbicacion()
.subscribe((data: Ubicacion[]) => {
this.Ubicacion = data;
});
this.SedeSer
.getSede()
.subscribe((data: Sede[]) => {
this.Sede = data;
});
   }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateLaboratorio(nombre, estado, tipo, ubicacion, sede ) {
   this.route.params.subscribe(params => {
      this.bs.updateLaboratorio(nombre, estado, tipo, ubicacion, sede  , params['id']);
      this.showInfo();
      this.router.navigate(['laboratorio']);
   });
}
}
