import { Component, OnInit } from '@angular/core';
import Laboratorio from '../../models/Laboratorio';
import { LaboratorioService } from '../../services/laboratorio.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-laboratorio-get',
  templateUrl: './laboratorio-get.component.html',
  styleUrls: ['./laboratorio-get.component.css']
})

export class LaboratorioGetComponent implements OnInit {

  laboratorio: Laboratorio[];

  constructor( private bs: LaboratorioService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getLaboratorio()
      .subscribe((data: Laboratorio[]) => {
        this.laboratorio = data;
    });
  }

  deleteLaboratorio(id) {
    this.bs.deleteLaboratorio(id).subscribe(res => {
      console.log('Deleted');
      this.bs
      .getLaboratorio()
      .subscribe((data: Laboratorio[]) => {
        this.laboratorio = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

