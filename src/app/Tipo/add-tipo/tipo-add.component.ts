import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { TipoService } from '../../services/tipo.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-tipo-add',
  templateUrl: './tipo-add.component.html',
  styleUrls: ['./tipo-add.component.css']
})
export class TipoAddComponent implements OnInit {
      angForm: FormGroup;
  constructor(

   private fb: FormBuilder,
   private Tipo_ser: TipoService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombre: ['', Validators.required ],
descripcion: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addTipo(nombre, descripcion ) {
    this.Tipo_ser.addTipo(nombre, descripcion );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
      }

}
