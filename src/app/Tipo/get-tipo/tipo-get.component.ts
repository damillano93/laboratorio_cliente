import { Component, OnInit } from '@angular/core';
import Tipo from '../../models/Tipo';
import { TipoService } from '../../services/tipo.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-tipo-get',
  templateUrl: './tipo-get.component.html',
  styleUrls: ['./tipo-get.component.css']
})

export class TipoGetComponent implements OnInit {

  tipo: Tipo[];

  constructor( private bs: TipoService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getTipo()
      .subscribe((data: Tipo[]) => {
        this.tipo = data;
    });
  }

  deleteTipo(id) {
    this.bs.deleteTipo(id).subscribe(res => {
      console.log('Deleted');
      this.bs
      .getTipo()
      .subscribe((data: Tipo[]) => {
        this.tipo = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

