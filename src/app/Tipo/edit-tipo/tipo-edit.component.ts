import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { TipoService } from '../../services/tipo.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-tipo-edit',
  templateUrl: './tipo-edit.component.html',
  styleUrls: ['./tipo-edit.component.css']
})
export class TipoEditComponent implements OnInit {
     angForm: FormGroup;
  tipo: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager,
    private bs: TipoService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombre: ['', Validators.required ],
descripcion: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editTipo(params['id']).subscribe(res => {
        this.tipo = res;
      });
    });
        }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateTipo(nombre, descripcion ) {
   this.route.params.subscribe(params => {
      this.bs.updateTipo(nombre, descripcion  , params['id']);
      this.showInfo();
      this.router.navigate(['tipo']);
   });
}
}
