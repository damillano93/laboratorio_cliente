import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Nombre_dia from '../../models/Nombre_dia';
import Laboratorio from '../../models/Laboratorio';
import Persona from '../../models/Persona';
import { DisponibilidadService } from '../../services/disponibilidad.service';
import { NombrediaService } from '../../services/nombre_dia.service';
import { LaboratorioService } from '../../services/laboratorio.service';
import { PersonaService } from '../../services/persona.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-calendario-edit',
  templateUrl: './calendario-edit.component.html',
  styleUrls: ['./calendario-edit.component.css']
})
export class CalendarioEditComponent implements OnInit {
  Nombre_dia: Nombre_dia[];
  Laboratorio: Laboratorio[];
  Persona: Persona[];
  angForm: FormGroup;
  disponibilidad: any = {};

  constructor(private route: ActivatedRoute,
    private Nombre_diaSer: NombrediaService,
    private LaboratorioSer: LaboratorioService,
    private PersonaSer: PersonaService,
    private router: Router,
    public toastr: ToastrManager,
    private bs: DisponibilidadService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        fecha: ['', Validators.required ],
dia: ['', Validators.required ],
nombre_dia:  ['', Validators.required ],
laboratorio: ['', Validators.required ],
descripcion: ['', Validators.required ],
responsable: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editDisponibilidad(params['id']).subscribe(res => {
        this.disponibilidad = res;
      });
    });
     this.Nombre_diaSer
.getNombre_dia()
.subscribe((data: Nombre_dia[]) => {
this.Nombre_dia = data;
});
this.LaboratorioSer
.getLaboratorio()
.subscribe((data: Laboratorio[]) => {
this.Laboratorio = data;
});
this.PersonaSer
.getPersona()
.subscribe((data: Persona[]) => {
this.Persona = data;
});



   }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateDisponibilidad(fecha, dia, nombre_dia, laboratorio, descripcion, responsable ) {
   this.route.params.subscribe(params => {
      this.bs.updateDisponibilidad(fecha, dia, nombre_dia, laboratorio, descripcion, responsable  , params['id']);
      this.showInfo();
      this.router.navigate(['calendario']);
   });
}
}
