
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import Disponibilidad from '../models/Disponibilidad';
import Persona from '../models/Persona';
import { DisponibilidadService } from '../services/disponibilidad.service';
import { PersonaService } from '../services/persona.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
selector: 'app-calendario',
templateUrl: './calendario.component.html',
styleUrls: ['./calendario.component.css']
})

export class CalendarioComponent implements OnInit {
angForm: FormGroup;
persona: Persona[];
disponibilidad: Disponibilidad[];
holidays: Disponibilidad[];
constructor(  private bs: DisponibilidadService, private PersonaSer: PersonaService, public toastr: ToastrManager ) { }


year: number[] = [];
selectedYear = 2019;
months: string[] = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo',
'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
monthNumber: number[] = [];
selectedMonth = 5 ;
weekday: string[] = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
dayInfo: { day: number, isHoliday: boolean, yearDay: number, weekday: string }[] = [];
holidayInfo: { date: string, day: string, holiday: string, description: string }[] = [];

// variables for function whih gives day of the year
selectedDate: any = '';
startDateOfTheyear: any = '';
dateDifference = 0;
oneDayTime = 0;
dayOfTheYear = 0;

// to get day of the year (eg: 28 February is 59th Day of the Year)
// logic taken from - 'https://stackoverflow.com/questions/8619879/javascript-calculate-the-day-of-the-year-1-366'
getDayOfTheyear(year, month, day) {
this.selectedDate = new Date(year, month, day);
this.startDateOfTheyear = new Date(this.selectedDate.getFullYear(), 0, 0);
this.dateDifference = (this.selectedDate - this.startDateOfTheyear) +
((this.startDateOfTheyear.getTimezoneOffset() - this.selectedDate.getTimezoneOffset()) * 60 * 1000);
this.oneDayTime = 1000 * 60 * 60 * 24;
this.dayOfTheYear = Math.floor(this.dateDifference / this.oneDayTime);

return this.dayOfTheYear;
}

// to get number of days in a selected Month
getDaysInMonth = function (month: number, year: number) {
return new Date(year, month, 0).getDate();
};

// to get days in a selected Month and Year
getDaysByMonthAndYear() {

this.holidayInfo = [];
const month = this.selectedMonth + 1;
const year = this.selectedYear;
this.dayInfo = [];

for (let i = 0; i < this.getDaysInMonth(month, year); i++) {
const temp = { day: 0, isHoliday: false, yearDay: 0, weekday: '' };
temp.day = i + 1;
temp.weekday = this.weekday[(new Date(year, month - 1, i + 1)).getDay()];
temp.yearDay = this.getDayOfTheyear(year, month - 1, temp.day);

const index = this.holidays.findIndex(x => x.dia === temp.yearDay);

if (index > -1 && year === 2019) {
temp.isHoliday = true;
}
this.dayInfo.push(temp);
}
}
getAllIndexes(arr, val) {
  const indexes = [];
  for (let i = 0; i < arr.length; i++) {
      if (arr[i].dia === val) {
          indexes.push(i);
      }
  }
  return indexes;
}

// get info of the chosen day
getInfoOfTheDay(year, month, day, yearDay) {
  const indexes = this.getAllIndexes(this.holidays, yearDay);
// const index = this.holidays.findIndex(x => x.dia === yearDay);
this.holidayInfo = [];
console.log('registros');
console.log(this.holidays);
for (let i = 0; i < indexes.length; i++) {
  const index = indexes[i];
  const info = { 'date': '', 'day': '', 'holiday': '', 'description': '', 'id': '' };
if (index > -1 && year === 2019) {
info.id = this.holidays[index]._id;
info.date = this.holidays[index].fecha;
info.day = this.holidays[index].nombre_dia;
info.holiday = this.holidays[index].laboratorio;
if (this.holidays[index].responsable !== 'nulo') {
  this.PersonaSer.editPersona(this.holidays[index].responsable).subscribe((data: Persona) => {
    console.log(data);
    info.description = data.nombre;
  });
} else {
    info.description = this.holidays[index].responsable;
}

} else {
info.date = this.months[month] + ' ' + day;
info.day = this.weekday[(new Date(year, month, day)).getDay()];
info.holiday = '-';
info.description = '-';
}

this.holidayInfo.push(info);
console.log(this.holidayInfo);
}

}

ngOnInit() {
    this.bs
    .getDisponibilidad()
    .subscribe((data: Disponibilidad[]) => {
      // this.disponibilidad = data;
      this.holidays = data;
      console.log( this.holidays);
      // init of getting days in a particular Month and Year
this.getDaysByMonthAndYear();
  });
// Month List Initialisation
for (let i = 0; i < 12; i++) {
this.monthNumber.push(i);
}

// year List Initialisation
for (let i = 2001; i < 2100; i++) {
this.year.push(i);
}


}


}
