import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CalendarioComponent } from './Calendario/calendario.component';
import { CalendarioEditComponent } from './Calendario/edit-calendario/calendario-edit.component';
import { TipoAddComponent } from './Tipo/add-tipo/tipo-add.component';
import { TipoEditComponent } from './Tipo/edit-tipo/tipo-edit.component';
import { TipoGetComponent } from './Tipo/get-tipo/tipo-get.component';
import { UbicacionAddComponent } from './Ubicacion/add-ubicacion/ubicacion-add.component';
import { UbicacionEditComponent } from './Ubicacion/edit-ubicacion/ubicacion-edit.component';
import { UbicacionGetComponent } from './Ubicacion/get-ubicacion/ubicacion-get.component';
import { SedeAddComponent } from './Sede/add-sede/sede-add.component';
import { SedeEditComponent } from './Sede/edit-sede/sede-edit.component';
import { SedeGetComponent } from './Sede/get-sede/sede-get.component';
import { DisponibilidadAddComponent } from './Disponibilidad/add-disponibilidad/disponibilidad-add.component';
import { DisponibilidadEditComponent } from './Disponibilidad/edit-disponibilidad/disponibilidad-edit.component';
import { DisponibilidadGetComponent } from './Disponibilidad/get-disponibilidad/disponibilidad-get.component';
import { LaboratorioAddComponent } from './Laboratorio/add-laboratorio/laboratorio-add.component';
import { LaboratorioEditComponent } from './Laboratorio/edit-laboratorio/laboratorio-edit.component';
import { LaboratorioGetComponent } from './Laboratorio/get-laboratorio/laboratorio-get.component';

import { ToastrModule } from 'ng6-toastr-notifications';
import { SidebarjsModule } from 'ng-sidebarjs';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TipoService } from './services/tipo.service';
import { UbicacionService } from './services/ubicacion.service';
import { SedeService } from './services/sede.service';
import { DisponibilidadService } from './services/disponibilidad.service';
import { LaboratorioService } from './services/laboratorio.service';


@NgModule({
  declarations: [
     HomeComponent,
     CalendarioComponent,
     CalendarioEditComponent,
    AppComponent,
TipoAddComponent,
TipoGetComponent,
TipoEditComponent
,
UbicacionAddComponent,
UbicacionGetComponent,
UbicacionEditComponent
,
SedeAddComponent,
SedeGetComponent,
SedeEditComponent
,
DisponibilidadAddComponent,
DisponibilidadGetComponent,
DisponibilidadEditComponent
,
LaboratorioAddComponent,
LaboratorioGetComponent,
LaboratorioEditComponent

  ],
  imports: [
    NgbModule,
    SidebarjsModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SlimLoadingBarModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    DataTablesModule,
    ToastrModule.forRoot()
  ],
  providers: [
    TipoService,
UbicacionService,
SedeService,
DisponibilidadService,
LaboratorioService

    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
