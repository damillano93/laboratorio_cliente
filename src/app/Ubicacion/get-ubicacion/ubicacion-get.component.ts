import { Component, OnInit } from '@angular/core';
import Ubicacion from '../../models/Ubicacion';
import { UbicacionService } from '../../services/ubicacion.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-ubicacion-get',
  templateUrl: './ubicacion-get.component.html',
  styleUrls: ['./ubicacion-get.component.css']
})

export class UbicacionGetComponent implements OnInit {

  ubicacion: Ubicacion[];

  constructor( private bs: UbicacionService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getUbicacion()
      .subscribe((data: Ubicacion[]) => {
        this.ubicacion = data;
    });
  }

  deleteUbicacion(id) {
    this.bs.deleteUbicacion(id).subscribe(res => {
      console.log('Deleted');
      this.bs
      .getUbicacion()
      .subscribe((data: Ubicacion[]) => {
        this.ubicacion = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

