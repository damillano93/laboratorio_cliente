import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { UbicacionService } from '../../services/ubicacion.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-ubicacion-edit',
  templateUrl: './ubicacion-edit.component.html',
  styleUrls: ['./ubicacion-edit.component.css']
})
export class UbicacionEditComponent implements OnInit {
     angForm: FormGroup;
  ubicacion: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager,
    private bs: UbicacionService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
direccion: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editUbicacion(params['id']).subscribe(res => {
        this.ubicacion = res;
      });
    });
        }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateUbicacion(nombre, descripcion, direccion ) {
   this.route.params.subscribe(params => {
      this.bs.updateUbicacion(nombre, descripcion, direccion  , params['id']);
      this.showInfo();
      this.router.navigate(['ubicacion']);
   });
}
}
