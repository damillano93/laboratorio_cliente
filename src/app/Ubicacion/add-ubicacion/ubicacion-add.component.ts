import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { UbicacionService } from '../../services/ubicacion.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-ubicacion-add',
  templateUrl: './ubicacion-add.component.html',
  styleUrls: ['./ubicacion-add.component.css']
})
export class UbicacionAddComponent implements OnInit {
      angForm: FormGroup;
  constructor(

   private fb: FormBuilder,
   private Ubicacion_ser: UbicacionService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
direccion: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addUbicacion(nombre, descripcion, direccion ) {
    this.Ubicacion_ser.addUbicacion(nombre, descripcion, direccion );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
      }

}
