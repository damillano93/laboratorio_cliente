import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { SedeService } from '../../services/sede.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-sede-edit',
  templateUrl: './sede-edit.component.html',
  styleUrls: ['./sede-edit.component.css']
})
export class SedeEditComponent implements OnInit {
     angForm: FormGroup;
  sede: any = {};

  constructor(private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager,
    private bs: SedeService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
direccion: ['', Validators.required ]
       });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editSede(params['id']).subscribe(res => {
        this.sede = res;
      });
    });
        }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateSede(nombre, descripcion, direccion ) {
   this.route.params.subscribe(params => {
      this.bs.updateSede(nombre, descripcion, direccion  , params['id']);
      this.showInfo();
      this.router.navigate(['sede']);
   });
}
}
