import { Component, OnInit } from '@angular/core';
import Sede from '../../models/Sede';
import { SedeService } from '../../services/sede.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-sede-get',
  templateUrl: './sede-get.component.html',
  styleUrls: ['./sede-get.component.css']
})

export class SedeGetComponent implements OnInit {

  sede: Sede[];

  constructor( private bs: SedeService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getSede()
      .subscribe((data: Sede[]) => {
        this.sede = data;
    });
  }

  deleteSede(id) {
    this.bs.deleteSede(id).subscribe(res => {
      console.log('Deleted');
      this.bs
      .getSede()
      .subscribe((data: Sede[]) => {
        this.sede = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

