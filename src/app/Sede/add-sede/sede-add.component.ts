import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { SedeService } from '../../services/sede.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-sede-add',
  templateUrl: './sede-add.component.html',
  styleUrls: ['./sede-add.component.css']
})
export class SedeAddComponent implements OnInit {
      angForm: FormGroup;
  constructor(

   private fb: FormBuilder,
   private Sede_ser: SedeService,
   public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       nombre: ['', Validators.required ],
descripcion: ['', Validators.required ],
direccion: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addSede(nombre, descripcion, direccion ) {
    this.Sede_ser.addSede(nombre, descripcion, direccion );
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
      }

}
