import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DisponibilidadService {

  uri = 'http://52.206.242.173:3001/disponibilidad';

  constructor(private http: HttpClient) { }
  addDisponibilidad(fecha , dia , nombre_dia , laboratorio , descripcion , responsable ) {
    const obj = {
      fecha: fecha,
dia: dia,
nombre_dia: nombre_dia,
laboratorio: laboratorio,
descripcion: descripcion,
responsable: responsable

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getDisponibilidad() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editDisponibilidad(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateDisponibilidad(fecha , dia , nombre_dia , laboratorio , descripcion , responsable , id) {

    const obj = {
      fecha: fecha,
dia: dia,
nombre_dia: nombre_dia,
laboratorio: laboratorio,
descripcion: descripcion,
responsable: responsable

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteDisponibilidad(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
