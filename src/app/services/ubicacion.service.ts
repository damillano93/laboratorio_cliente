import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UbicacionService {

  uri = 'http://52.206.242.173:3001/ubicacion';

  constructor(private http: HttpClient) { }
  addUbicacion(nombre , descripcion , direccion ) {
    const obj = {
      nombre: nombre,
descripcion: descripcion,
direccion: direccion

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getUbicacion() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editUbicacion(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateUbicacion(nombre , descripcion , direccion , id) {

    const obj = {
      nombre: nombre,
descripcion: descripcion,
direccion: direccion

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteUbicacion(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
