import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LaboratorioService {

  uri = 'http://52.206.242.173:3001/laboratorio';

  constructor(private http: HttpClient) { }
  addLaboratorio(nombre , estado , tipo , ubicacion , sede ) {
    const obj = {
      nombre: nombre,
estado: estado,
tipo: tipo,
ubicacion: ubicacion,
sede: sede

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getLaboratorio() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editLaboratorio(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateLaboratorio(nombre , estado , tipo , ubicacion , sede , id) {

    const obj = {
      nombre: nombre,
estado: estado,
tipo: tipo,
ubicacion: ubicacion,
sede: sede

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteLaboratorio(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
