import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NombrediaService {

  uri = 'http://52.206.242.173:3001/nombre_dia';

  constructor(private http: HttpClient) { }
  addNombre_dia(nombre ) {
    const obj = {
      nombre: nombre

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getNombre_dia() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editNombre_dia(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateNombre_dia(nombre , id) {

    const obj = {
      nombre: nombre

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteNombre_dia(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
