import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SedeService {

  uri = 'http://52.206.242.173:3001/sede';

  constructor(private http: HttpClient) { }
  addSede(nombre , descripcion , direccion ) {
    const obj = {
      nombre: nombre,
descripcion: descripcion,
direccion: direccion

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getSede() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editSede(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateSede(nombre , descripcion , direccion , id) {

    const obj = {
      nombre: nombre,
descripcion: descripcion,
direccion: direccion

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteSede(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
