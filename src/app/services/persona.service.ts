import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  uri = 'http://54.158.213.16:3002/persona';

  constructor(private http: HttpClient) { }
  addPersona(nombre , descripcion , direccion ) {
    const obj = {
      nombre: nombre,
descripcion: descripcion,
direccion: direccion

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getPersona() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editPersona(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updatePersona(nombre , descripcion , direccion , id) {

    const obj = {
      nombre: nombre,
descripcion: descripcion,
direccion: direccion

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deletePersona(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
