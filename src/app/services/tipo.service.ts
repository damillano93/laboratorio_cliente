import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TipoService {

  uri = 'http://52.206.242.173:3001/tipo';

  constructor(private http: HttpClient) { }
  addTipo(nombre , descripcion ) {
    const obj = {
      nombre: nombre,
descripcion: descripcion

    };
    this.http.post(`${this.uri}`, obj)
        .subscribe(res => console.log('Done'));
  }
  getTipo() {
    return this
           .http
           .get(`${this.uri}`);
  }
  editTipo(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }
  updateTipo(nombre , descripcion , id) {

    const obj = {
      nombre: nombre,
descripcion: descripcion

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }
 deleteTipo(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
